<?php


namespace GuanChanghu\Configs;

/**
 * Class FieldConfig
 * @package GuanChanghu\Configs
 */
class FieldConfig
{
    /**
     * boolean true
     */
    public const BOOLEAN_TRUE = 1;

    /**
     * boolean false
     */
    public const BOOLEAN_FALSE = 0;

    /**
     * 状态-进行中
     */
    public const STATUS_UNDERWAY = 0;

    /**
     * 状态标记-成功
     */
    public const STATUS_SUCCESS = 1;

    /**
     * 状态标记-失败
     */
    public const STATUS_FAIL = 2;

    /**
     * 是否成功字段
     */
    public const IS_SUCCESS_FIELD = 'IS_SUCCESS';

    /**
     * 消息字段
     */
    public const MESSAGE_FIELD = 'message';

    /**
     * 数据字段
     */
    public const DATA_FIELD = 'data';

    /**
     * 是否显示-是
     */
    public const IS_SHOW_YES = self::BOOLEAN_TRUE;

    /**
     * 是否显示-否
     */
    public const IS_SHOW_NO = self::BOOLEAN_FALSE;

    /**
     * 是否阅读过-是
     */
    public const IS_READ_YES = self::BOOLEAN_TRUE;

    /**
     * 是否阅读过-否
     */
    public const IS_READ_NO = self::BOOLEAN_FALSE;

    /**
     * 是否点赞-是
     */
    public const IS_LIKE_YES = self::BOOLEAN_TRUE;

    /**
     * 是否点赞-否
     */
    public const IS_LIKE_NO = self::BOOLEAN_FALSE;

    /**
     * 是否踩-是
     */
    public const IS_TRAMPLE_YES = self::BOOLEAN_TRUE;

    /**
     * 是否踩-否
     */
    public const IS_TRAMPLE_NO = self::BOOLEAN_FALSE;

    /**
     * 是否置顶-是
     */
    public const IS_TOP_YES = self::BOOLEAN_TRUE;

    /**
     * 是否置顶-否
     */
    public const IS_TOP_NO = self::BOOLEAN_FALSE;

    /**
     * 是否收藏-是
     */
    public const IS_COLLECT_YES = self::BOOLEAN_TRUE;

    /**
     * 是否收藏-否
     */
    public const IS_COLLECT_NO = self::BOOLEAN_FALSE;


    /**
     * 是否关注-是
     */
    public const IS_ATTENTION_YES = self::BOOLEAN_TRUE;

    /**
     * 是否关注-否
     */
    public const IS_ATTENTION_NO = self::BOOLEAN_FALSE;

    /**
     * 是否分享-是
     */
    public const IS_SHARE_YES = self::BOOLEAN_TRUE;

    /**
     * 是否分享-否
     */
    public const IS_SHARE_NO = self::BOOLEAN_FALSE;

    /**
     * 是否评价-是
     */
    public const IS_COMMENT_YES = self::BOOLEAN_TRUE;

    /**
     * 是否评价-否
     */
    public const IS_COMMENT_NO = self::BOOLEAN_FALSE;

    /**
     * 开启
     */
    public const IS_ENABLED_YES = self::BOOLEAN_TRUE;

    /**
     * 关闭
     */
    public const IS_ENABLED_NO = self::BOOLEAN_FALSE;

    /**
     * 是否完成-完成
     */
    public const IS_FINISH_YES = self::BOOLEAN_TRUE;

    /**
     * 是否完成-没有
     */
    public const IS_FINISH_NO = self::BOOLEAN_FALSE;
}
