<?php


namespace GuanChanghu\Configs;

/**
 * @author 管昌虎
 * Class SerialConfig
 * @package GuanChanghu\Configs
 * Created on 2022/3/31 9:09
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class SerialConfig
{
    /**
     * 阿拉伯序号
     */
    public const ARABIC_DICT = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',];

    /**
     * 身份证号最后一位
     */
    public const ID_NUMBER_LAST_ONE_DICT = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'X'];

    /**
     * 中文序号
     */
    public const CHINESE_DICT = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九'];

    /**
     * 英文字母
     */
    public const ENGLISH_UPPER_DICT = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
}
