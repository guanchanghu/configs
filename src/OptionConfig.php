<?php


namespace GuanChanghu\Configs;

/**
 * Class OptionConfig
 * @package GuanChanghu\Configs
 */
class OptionConfig
{
    /**
     * 暂未使用,默认都是抛出异常的
     * 抛出异常
     */
    public const OPTIONS_WITH_THROWABLE = 1;

    /**
     * 使用锁
     */
    public const OPTIONS_WITH_LOCK = 2;
}
