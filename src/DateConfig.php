<?php


namespace GuanChanghu\Configs;

/**
 * Class DateConfig
 * @package GuanChanghu\Configs
 */
class DateConfig
{
    /**
     * 时间格式- 年-月-日 时:分:秒.毫秒
     */
    public const DATE_FORMAT_DATA_TIME_MILLISECOND = 'Y-m-d H:i:s.u';

    /**
     * 时间格式- 年-月-日 时:分:秒
     */
    public const DATE_FORMAT_DATA_TIME = 'Y-m-d H:i:s';

    /**
     * 时间格式- 年-月-日
     */
    public const DATE_FORMAT_DATA = 'Y-m-d';

    /**
     * 时间格式- 年-月
     */
    public const DATE_FORMAT_DATA_MONTH = 'Y-m';

    /**
     * 时间格式- 年月日
     */
    public const DATE_FORMAT_SIMPLE_DATA = 'Ymd';

    /**
     * 时间格式- 时:分:秒
     */
    public const DATE_FORMAT_TIME = 'H:i:s';

    /**
     * 未开始
     */
    public const STATUS_NOT_START = 10;

    /**
     * 已开始
     */
    public const STATUS_UNDERWAY = 20;

    /**
     * 已结束
     */
    public const STATUS_FINISHED = 30;
}
