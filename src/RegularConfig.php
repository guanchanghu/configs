<?php
/**
 * 正则配置
 * Created on 2022/4/29 10:46
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Configs;

/**
 * @author 管昌虎
 * Class RegularConfig
 * @package GuanChanghu\Configs
 * Created on 2022/4/29 10:46
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class RegularConfig
{
    /**
     * 手机号正则
     */
    public const MOBILE = '/^((\(\d{2,3}\))|(\d{3}\-))?1\d{10}$/';

    /**
     * 邮箱正则
     */
    public const EMAIL = '/^(\w|\d)+([-+.](\w|\d)+)*@(\w|\d)+([-.](\w|\d)+)*\.\w+([-.]\w+)*/';

    /**
     * 图片正则
     */
    public const IMAGE = '/.*(\.png|\.jpg|\.jpeg|\.gif)$/';

    /**
     * 用户名正则
     */
    public const USERNAME = '/^[a-z][a-z0-9]{2,16}$/';

    /**
     * 密码正则
     */
    public const PASSWORD = '/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/';

    /**
     * 安全密码正则
     */
    public const SAFE_PASSWORD = '/^\d{6}$/';
}
