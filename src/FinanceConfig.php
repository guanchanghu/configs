<?php
/**
 * 财务配置
 * Created on 2022/3/31 9:21
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Configs;

/**
 * @author 管昌虎
 * Class FinanceConfigHandle
 * @package GuanChanghu\Configs
 * Created on 2022/3/31 9:21
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class FinanceConfig
{
    /**
     * 手续费手续方式-内扣
     */
    public const FINANCE_SERVICE_CHARGE_WAY_INNER = 0;

    /**
     * 手续费手续方式-外扣
     */
    public const FINANCE_SERVICE_CHARGE_WAY_OUTSIDE = 1;
}
